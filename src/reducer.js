import { LAP_TIMER, RESET_TIMER, START_TIMER, STOP_TIMER } from './actions';

export const initialState = {
  isRunning: false,
  isPaused: false,
  start: 0,
  milliseconds: 0,
  laps: []
};

export const reducer = (state = initialState, action) => {
  if (action.type === START_TIMER) {
    return {
      ...state,
      isRunning: true,
      isPaused: false,
      milliseconds: (state.milliseconds += action.payload.deltaTime)
    };
  }
  if (action.type === STOP_TIMER) {
    return {
      ...state,
      isPaused: true
    };
  }
  if (action.type === RESET_TIMER) {
    return {
      isRunning: false,
      isPaused: false,
      milliseconds: 0,
      laps: []
    };
  }
  if (action.type === LAP_TIMER) {
    return {
      ...state,
      laps: [...state.laps, action.payload]
    };
  }

  return state;
};
