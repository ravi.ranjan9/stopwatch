export const START_TIMER = 'START_TIMER';
export const STOP_TIMER = 'STOP_TIMER';
export const LAP_TIMER = 'LAP_TIMER';
export const RESET_TIMER = 'RESET_TIMER';

export const startTimer = (value = 0) => ({
  type: START_TIMER,
  payload: { deltaTime: value }
});
export const stopTimer = () => ({ type: STOP_TIMER });
export const resetTimer = () => ({ type: RESET_TIMER });
export const lapTimer = (value) => ({ type: LAP_TIMER, payload: value });
