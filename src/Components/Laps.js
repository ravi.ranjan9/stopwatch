import { useSelector } from 'react-redux';
import '../App.css';

export default function Laps() {
  const laps = useSelector((state) => state.laps);

  return (
    <ol className="lap-list">
      {laps.map((time) => (
        <li key={time[0].id}>{time[0].value}</li>
      ))}
    </ol>
  );
}
