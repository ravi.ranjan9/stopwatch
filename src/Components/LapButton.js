import { useDispatch, useSelector } from 'react-redux';
import { lapTimer } from '../actions';
import '../App.css';

let id = 0;

export default function LapButton() {
  const dispatch = useDispatch();
  const isRunning = useSelector((state) => state.isRunning);
  const isPaused = useSelector((state) => state.isPaused);
  let ms = useSelector((state) => state.milliseconds);
  let sec = Math.floor(ms / 1000);
  let hrs = Math.floor(sec / 3600);
  let min = Math.floor(sec / 60);
  let lap = [{ id: ++id, value: `${hrs}:${min}:${sec}:${ms % 100}` }];
  return (
    <button
      disabled={!isRunning || isPaused}
      onClick={() => dispatch(lapTimer(lap))}
    >
      Lap
    </button>
  );
}
