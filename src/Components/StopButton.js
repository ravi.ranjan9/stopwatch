import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import { stopTimer } from '../actions';
import '../App.css';

export default function StopButton() {
  const dispatch = useDispatch();
  const isRunning = useSelector((state) => state.isRunning);
  return (
    <button disabled={!isRunning} onClick={() => dispatch(stopTimer())}>
      Stop
    </button>
  );
}
