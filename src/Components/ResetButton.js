import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import { resetTimer } from '../actions';
import '../App.css';

export default function ResetButton() {
  const dispatch = useDispatch();
  const isRunning = useSelector((state) => state.isRunning);

  return (
    <button disabled={!isRunning} onClick={() => dispatch(resetTimer())}>
      Reset
    </button>
  );
}
