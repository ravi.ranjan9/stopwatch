import StartButton from './StartButton';
import LapButton from './LapButton';
import ResetButton from './ResetButton';
import StopButton from './StopButton';
import Laps from './Laps';
import '../App.css';
import { useSelector } from 'react-redux';
import { useEffect } from 'react';

import { useDispatch } from 'react-redux';
import { startTimer } from '../actions';

export default function StopWatchTimer() {
  const dispatch = useDispatch();
  let lastUpdateTime = Date.now();
  let ms = useSelector((state) => state.milliseconds);
  let centisec = Math.floor(ms / 10);
  let sec = Math.floor(ms / 1000);
  let hrs = Math.floor(sec / 3600);
  let min = Math.floor(sec / 60);

  const isRunning = useSelector((state) => state.isRunning);
  const isPaused = useSelector((state) => state.isPaused);

  useEffect(() => {
    if (isRunning) {
      const timer = setInterval(() => {
        if (!isPaused) {
          const now = Date.now();
          const deltaTime = now - lastUpdateTime;
          lastUpdateTime = now;
          dispatch(startTimer(deltaTime));
        }
      }, 50);
      return () => {
        window.clearInterval(timer);
      };
    }
  }, [dispatch, isPaused, isRunning, centisec]);

  return (
    <div className="timer-container">
      <div className="timer">
        <span className="hrs">{hrs % 24}:</span>
        <span className="min">{min % 60}:</span>
        <span className="sec">{sec % 60}:</span>
        <span className="centisec">{centisec % 100}</span>
      </div>
      <div className="buttons">
        <StartButton />
        <LapButton />
        <ResetButton />
        <StopButton />
      </div>
      <Laps />
    </div>
  );
}
