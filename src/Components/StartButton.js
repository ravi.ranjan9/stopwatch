import { useDispatch, useSelector } from 'react-redux';
import { startTimer } from '../actions';
import '../App.css';

export default function StartButton() {
  const dispatch = useDispatch();
  const isRunning = useSelector((state) => state.isRunning);
  const isPaused = useSelector((state) => state.isPaused);
  return (
    <button
      disabled={isRunning && !isPaused}
      onClick={() => dispatch(startTimer())}
    >
      Start
    </button>
  );
}
