import './App.css';
import StopWatchTimer from './Components/StopWatchTimer';
import { useState } from 'react';

export default function App() {
  const [timer, setTimer] = useState();
  return (
    <div className="App">
      <div className="container">
        <span className="heading">StopWatch</span>
        <StopWatchTimer />
      </div>
    </div>
  );
}
